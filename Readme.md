使用场景：
进入 maintenance mode 而非直接 disable 掉 Store View，是比较妥当的做法

实现手段：
新建一个 Router，用 Event 能达到同样效果吗？
没有在 Stores 页面新增设置项
在 config.xml 动了 <stores>


# [Store Maintenance](https://github.com/WineAwesomeness/BalkeTechnologies_StoreMaintenance)

Store Maintenance allows you to put each store view separately into maintenance mode. 根本找不到设置项在哪
It can be configured from backend, so no need to hack any of your files (e.g. index.php) on your server.


## Description
- - -
Store Maintenance is developed to give shop developers/administrators an easy way to lock out customers, while they maintain their stores.

## Features
- - -
 * A custom html page can be defined in the backend, which is displayed to all customers, while the store is in maintenance mode.
 * After logging into the backend as an administrator, the frontend can be used with this account. This way updates and changes to the shop / design can be reviewed and tested in a safe manner.
 * IPs can be white listed from within the backend, so chosen accounts can view the store frontend while in maintenance mode, without having to have administrator access.

## Compatibility
- - -
 * Magento >= 1.4

Configure and activate the extension under System - Balke Technologies - Store Maintenance

## Uninstallation Instructions
- - -
1. Delete the file app/etc/modules/BalkeTechnologies_StoreMaintenance.xml
2. Remove all remaining extension files:
    * app/code/community/BalkeTechnologies/StoreMainenance/
    * app/design/adminhtml/default/default/template/balketechnologies/storemaintenance/
    * app/design/adminhtml/base/default/template/balketechnologies/storemaintenance/
    * skin/frontend/base/default/balketechnologies/storemaintenance/
    * skin/frontend/default/default/balketechnologies/storemaintenance/
3. Execute this SQL to delete module settings from database:  
```
:::sql
DELETE FROM core_config_data WHERE path LIKE 'storeMaintenance%';
```


